﻿using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Win32.SafeHandles;

namespace CsharpDecoratorPattern.IOExample
{
    class Program
    {
        static void Main(string[] args)
        {
            const string documentPath = "./demo.txt";

            using (var fileStream = new FileStream(documentPath, FileMode.Create))
            {
                using var gzipStream = new GZipStream(fileStream, CompressionMode.Compress);
                using var writer = new StreamWriter(gzipStream);

                writer.WriteLine("I wrote this content to a file.");
            }

            string output; 
            using (var fileStream = new FileStream(documentPath, FileMode.Open))
            {
                using var gzipStream = new GZipStream(fileStream, CompressionMode.Decompress);
                using var reader = new StreamReader(gzipStream);
                output = reader.ReadLine();
            }

            Console.WriteLine(output);
        }
    }
}
