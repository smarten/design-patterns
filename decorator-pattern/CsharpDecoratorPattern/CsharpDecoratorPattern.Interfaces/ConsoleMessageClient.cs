﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpDecoratorPattern.Interfaces
{
    public class ConsoleMessageClient : IMessageClient
    {
        public void SendMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
