﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpDecoratorPattern.Interfaces
{
    public interface IMessageClient
    {
        void SendMessage(string message);
    }
}
