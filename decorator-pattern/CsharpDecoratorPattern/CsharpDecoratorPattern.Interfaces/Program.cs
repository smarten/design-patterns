﻿using System;
using System.IO;
using CsharpDecoratorPattern.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Extensions.Logging;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace CsharpDecoratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var logPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.Parent + "./logs.txt";

            var loggerFactory = LoggerFactory.Create(builder =>
                builder.AddSerilog(new LoggerConfiguration()
                    .WriteTo.File(logPath)
                    .MinimumLevel.Information()
                    .CreateLogger()
                ));
            var messageClientLogger = loggerFactory.CreateLogger<LoggerMessageClient>();
            
            var messageClient = new LoggerMessageClient(new TimestampMessageClient(new ConsoleMessageClient()), messageClientLogger);

            Console.WriteLine("Type 'exit' to close the program at any time.");
            while (true)
            {
                Console.Write("\nPlease write a message: ");
                var input = Console.ReadLine();

                if (input == "exit")
                {
                    break;
                }

                messageClient.SendMessage(input);
            }
        }
    }
}
