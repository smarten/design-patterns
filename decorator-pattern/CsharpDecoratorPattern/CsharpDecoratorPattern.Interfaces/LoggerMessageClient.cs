﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace CsharpDecoratorPattern.Interfaces
{
    public class LoggerMessageClient : IMessageClient
    {
        private readonly IMessageClient _inner;
        private readonly ILogger<LoggerMessageClient> _logger;

        public LoggerMessageClient(IMessageClient inner, ILogger<LoggerMessageClient> logger)
        {
            _inner = inner;
            _logger = logger;
        }

        public void SendMessage(string message)
        {
           _logger.LogInformation(message); 
           _inner.SendMessage(message);
        }
    }
}
