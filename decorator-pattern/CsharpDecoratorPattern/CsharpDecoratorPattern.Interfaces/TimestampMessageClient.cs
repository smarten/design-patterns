﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpDecoratorPattern.Interfaces
{
    public class TimestampMessageClient  : IMessageClient
    {
        private readonly IMessageClient _inner;

        public TimestampMessageClient(IMessageClient inner)
        {
            _inner = inner;
        }

        public void SendMessage(string message)
        {
           Console.Write(DateTime.Now.ToString("g") + " | ");
           _inner.SendMessage(message);
        }
    }
}
