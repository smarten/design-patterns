﻿using System;
using CsharpStrategyPattern.WithInterface.strategies;

namespace CsharpStrategyPattern.WithInterface
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var plane = new JetPlane(new FlyStrategy(), "Transavia");
            plane.Travel();

            plane.TravelStrategy = new CrashStrategy();
            plane.Travel();
        }
    }
}
