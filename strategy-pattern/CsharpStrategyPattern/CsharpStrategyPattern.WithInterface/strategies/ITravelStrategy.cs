﻿namespace CsharpStrategyPattern.WithInterface.strategies
{
    public interface ITravelStrategy
    {
        void Travel();
    }
}
