﻿using System;

namespace CsharpStrategyPattern.WithInterface.strategies
{
    public class CrashStrategy : ITravelStrategy
    {
        public void Travel()
        {
            Console.WriteLine("I'm crashing!!!");
        }
    }
}
