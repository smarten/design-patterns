﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CsharpStrategyPattern.WithInterface.strategies
{
    public class FlyStrategy : ITravelStrategy
    {
        public void Travel()
        {
            Console.WriteLine("I'm flying!");
        }
    }
}
