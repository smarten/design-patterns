﻿using CsharpStrategyPattern.WithInterface.strategies;

namespace CsharpStrategyPattern.WithInterface
{
    public class JetPlane
    {
        public ITravelStrategy TravelStrategy { get; set; }
        public string Airline { get; set; }

        public JetPlane(ITravelStrategy travelStrategy, string airline)
        {
            TravelStrategy = travelStrategy;
            Airline = airline;
        }

        public void Travel()
        {
            TravelStrategy.Travel();
        }
    }
}
