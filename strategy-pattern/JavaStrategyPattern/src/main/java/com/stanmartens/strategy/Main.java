package com.stanmartens.strategy;

import com.stanmartens.strategy.strategies.CrashStrategy;
import com.stanmartens.strategy.strategies.FlyStrategy;

public class Main {

    public static void main(String[] args) {
        JetPlane plane = new JetPlane(new FlyStrategy(), "Transavia");
        plane.travel();

        plane.setTravelStrategy(new CrashStrategy());
        plane.travel();
    }
}
