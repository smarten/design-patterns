package com.stanmartens.strategy.strategies;

public class FlyStrategy implements TravelStrategy {

    @Override
    public void travel() {
        System.out.println("I'm flying!");
    }
}
