package com.stanmartens.strategy.strategies;

public interface TravelStrategy {

    void travel();
}
