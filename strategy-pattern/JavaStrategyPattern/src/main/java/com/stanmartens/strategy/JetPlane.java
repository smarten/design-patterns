package com.stanmartens.strategy;

import com.stanmartens.strategy.strategies.TravelStrategy;

public class JetPlane {

    private TravelStrategy travelStrategy;
    private String airline;

    public JetPlane(TravelStrategy travelStrategy, String airline) {
        this.travelStrategy = travelStrategy;
        this.airline = airline;
    }

    public TravelStrategy getTravelStrategy() {
        return travelStrategy;
    }

    public void setTravelStrategy(TravelStrategy travelStrategy) {
        this.travelStrategy = travelStrategy;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public void travel() {
        this.travelStrategy.travel();
    }

}