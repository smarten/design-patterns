package com.stanmartens.strategy.strategies;

public class CrashStrategy implements TravelStrategy {

    @Override
    public void travel() {
        System.out.println("I'm crashing!!!!!");
    }
}
