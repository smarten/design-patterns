﻿using CsharpObserverPattern.Events.WeatherReporters;

namespace CsharpObserverPattern.Events
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentSeason = Season.Spring;
            var weatherStation = new WeatherStation();

            var spanishReporter = new SpanishWeatherReporter();
            var englishReporter = new EnglishWeatherReporter();

            weatherStation.WeatherMeasured += spanishReporter.OnWeatherMeasured;
            weatherStation.WeatherMeasured += englishReporter.OnWeatherMeasured;

            weatherStation.MeasureWeather(currentSeason);

            weatherStation.WeatherMeasured -= spanishReporter.OnWeatherMeasured;

            weatherStation.MeasureWeather(currentSeason);
        }
    }
}
