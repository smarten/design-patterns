﻿namespace CsharpObserverPattern.Events
{
    public enum Season
    {
        Spring,
        Summer,
        Autumn,
        Winter
    }
}
