﻿using System;
using System.Linq;

namespace CsharpObserverPattern.Events
{
    public class WeatherStation
    {
        private event EventHandler<WeatherMeasuredEventArgs> weatherMeasured;

        public event EventHandler<WeatherMeasuredEventArgs> WeatherMeasured
        {
            add
            {
                if (weatherMeasured == null || !weatherMeasured.GetInvocationList().Contains(value))
                {
                    weatherMeasured += value;
                }
            }
            remove => weatherMeasured -= value;
        }

        public void MeasureWeather(Season season)
        {
            var random = new Random();
            var temperature = 0;
            switch (season)
            {
                case Season.Spring:
                    temperature = random.Next(15, 25);
                    break;
                case Season.Summer:
                    temperature = random.Next(20, 30);
                    break;
                case Season.Autumn:
                    temperature = random.Next(5, 15);
                    break;
                case Season.Winter:
                    temperature = random.Next(-5, 10);
                    break;
            }

            var weatherData = new WeatherData(temperature);
            OnWeatherMeasured(new WeatherMeasuredEventArgs(weatherData));
        }

        private void OnWeatherMeasured(WeatherMeasuredEventArgs args)
        {
            // Thread-safe copy
            var temp = weatherMeasured;

            temp?.Invoke(this, args);
        }
    }
}
