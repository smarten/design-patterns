﻿using System;

namespace CsharpObserverPattern.Events
{
    public class WeatherMeasuredEventArgs : EventArgs
    {
        public WeatherData WeatherData { get; }

        public WeatherMeasuredEventArgs(WeatherData weatherData)
        {
            WeatherData = weatherData;
        }
    }
}
