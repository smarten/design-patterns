﻿using System;

namespace CsharpObserverPattern.Events.WeatherReporters
{
    public class SpanishWeatherReporter : BaseWeatherReporter
    {
        protected override void ReportWeather(WeatherData weatherData)
        {
            Console.WriteLine($"La temperatura de hoy es {weatherData.Temperature} °C.");
        }
    }
}
