﻿namespace CsharpObserverPattern.Events.WeatherReporters
{
    public abstract class BaseWeatherReporter
    {
        public void OnWeatherMeasured(object sender, WeatherMeasuredEventArgs args)
        {
            ReportWeather(args.WeatherData);
        }

        protected abstract void ReportWeather(WeatherData weatherData);
    }
}
