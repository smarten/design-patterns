﻿using System;

namespace CsharpObserverPattern.Events.WeatherReporters
{
    public class EnglishWeatherReporter : BaseWeatherReporter
    {
        protected override void ReportWeather(WeatherData weatherData)
        {
            Console.WriteLine($"The temperature for today is {weatherData.Temperature} °C.");
        }
    }
}
