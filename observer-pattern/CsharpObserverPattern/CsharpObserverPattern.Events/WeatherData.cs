﻿using System;

namespace CsharpObserverPattern.Events
{
    public class WeatherData
    {
        public int Temperature { get; }
        public DateTime PublishDate { get; }

        public WeatherData(int temperature)
        {
            Temperature = temperature;
            PublishDate = DateTime.Now;
        }
    }
}
