﻿using System;

namespace CsharpObserverPattern.Interfaces
{
    public class WeatherData
    {
        public double Temperature { get; }
        public DateTime PublishDate { get; }

        public WeatherData(double temperature)
        {
            Temperature = temperature;
            PublishDate = DateTime.Now;
        }
    }
}
