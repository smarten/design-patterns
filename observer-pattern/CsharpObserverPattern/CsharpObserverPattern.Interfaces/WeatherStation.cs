﻿using System;
using System.Collections.Generic;

namespace CsharpObserverPattern.Interfaces
{
    public class WeatherStation : IObservable<WeatherData>
    {
        private readonly List<IObserver<WeatherData>> _observers;
        private readonly List<WeatherData> _registeredWeatherReports;

        public WeatherStation()
        {
            _observers = new List<IObserver<WeatherData>>();
            _registeredWeatherReports = new List<WeatherData>();

        }

        public IDisposable Subscribe(IObserver<WeatherData> observer)
        {
            if (!_observers.Contains(observer))
            {
                _observers.Add(observer);
            }

            return new Unsubscriber<WeatherData>(_observers, observer);
        }

        public void ReleaseWeatherData(Season season)
        {
            var random = new Random();
            int temperature = 0;
            switch (season)
            {
                case Season.Spring:
                    temperature = random.Next(15, 25);
                    break;
                case Season.Summer:
                    temperature = random.Next(20, 30);
                    break;
                case Season.Autumn:
                    temperature = random.Next(5, 15);
                    break;
                case Season.Winter:
                    temperature = random.Next(-5, 10);
                    break;
            }

            var weatherData = new WeatherData(temperature);
            _registeredWeatherReports.Add(weatherData);

            _observers.ForEach(observer => observer.OnNext(weatherData));
        }
    }
}
    