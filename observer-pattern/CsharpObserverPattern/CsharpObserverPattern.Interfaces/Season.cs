﻿namespace CsharpObserverPattern.Interfaces
{
    public enum Season
    {
        Spring,
        Summer,
        Autumn,
        Winter
    }
}
