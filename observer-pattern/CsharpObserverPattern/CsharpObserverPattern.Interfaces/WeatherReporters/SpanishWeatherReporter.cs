﻿using System;

namespace CsharpObserverPattern.Interfaces.WeatherReporters
{
    public class SpanishWeatherReporter : BaseWeatherReporter
    {
        public override void ReportWeather(WeatherData weatherData)
        {
            Console.WriteLine($"La temperatura de hoy es {weatherData.Temperature} °C.");
        }
    }
}
