﻿using System;

namespace CsharpObserverPattern.Interfaces.WeatherReporters
{
    public class EnglishWeatherReporter : BaseWeatherReporter
    {
        public override void ReportWeather(WeatherData weatherData)
        {
            Console.WriteLine($"The temperature for today is {weatherData.Temperature} °C.");
        }
    }
}
