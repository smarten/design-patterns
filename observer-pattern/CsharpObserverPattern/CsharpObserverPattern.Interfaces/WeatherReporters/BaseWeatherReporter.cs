﻿using System;

namespace CsharpObserverPattern.Interfaces.WeatherReporters
{
    public abstract class BaseWeatherReporter : IObserver<WeatherData>
    {
        private IDisposable Unsubscriber;

        public virtual void OnCompleted()
        {
            // Not implemented
        }

        public virtual void OnError(Exception error)
        {
            // Not implemented
        }

        public virtual void OnNext(WeatherData weatherData)
        {
            ReportWeather(weatherData);
        }

        public void Subscribe(IObservable<WeatherData> observable)
        {
            Unsubscriber = observable.Subscribe(this);
        }

        public void Unsubscribe()
        {
            Unsubscriber?.Dispose();
        }

        public abstract void ReportWeather(WeatherData weatherData);
    }
}
