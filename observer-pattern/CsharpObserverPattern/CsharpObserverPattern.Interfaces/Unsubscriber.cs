﻿using System;
using System.Collections.Generic;

namespace CsharpObserverPattern.Interfaces
{
    public class Unsubscriber<T> : IDisposable
    {
        private readonly List<IObserver<T>> _observers;
        private readonly IObserver<T> _observerToUnsubscribe;

        public Unsubscriber(List<IObserver<T>> observers, IObserver<T> observerToUnsubscribe)
        {
            _observers = observers;
            _observerToUnsubscribe = observerToUnsubscribe;
        }

        public void Dispose()
        {
            _observers.Remove(_observerToUnsubscribe);
        }
    }
}
