﻿using CsharpObserverPattern.Interfaces.WeatherReporters;

namespace CsharpObserverPattern.Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentSeason = Season.Spring;

            var weatherStation = new WeatherStation();

            var spanishReporter = new SpanishWeatherReporter();
            var englishReporter = new EnglishWeatherReporter();

            spanishReporter.Subscribe(weatherStation);
            englishReporter.Subscribe(weatherStation);

            weatherStation.ReleaseWeatherData(currentSeason);

            spanishReporter.Unsubscribe();

            weatherStation.ReleaseWeatherData(currentSeason);
        }
    }
}
