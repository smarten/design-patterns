package com.stanmartens.observer.interfaces;

public enum Season {
    SPRING,
    SUMMER,
    AUTUMN,
    WINTER
}
