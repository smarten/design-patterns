package com.stanmartens.observer.builtinobserver.weatherreporters;

import com.stanmartens.observer.builtinobserver.WeatherData;

public class EnglishWeatherReporter extends BaseWeatherReporter {

    @Override
    protected void reportWeather(WeatherData weatherData) {
        System.out.println(String.format("The temperature for today is %d °C.", weatherData.getTemperature()));
    }
}
