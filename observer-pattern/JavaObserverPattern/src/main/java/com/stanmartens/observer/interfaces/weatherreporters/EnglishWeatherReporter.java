package com.stanmartens.observer.interfaces.weatherreporters;

import com.stanmartens.observer.interfaces.WeatherData;

public class EnglishWeatherReporter extends BaseWeatherReporter {

    @Override
    protected void reportWeather(WeatherData data) {
        System.out.println(String.format("The temperature for today is %d °C", data.getTemperature()));
    }
}
