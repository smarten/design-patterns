package com.stanmartens.observer.builtinobserver.weatherreporters;

import com.stanmartens.observer.builtinobserver.WeatherData;

import java.util.Observable;
import java.util.Observer;

public abstract class BaseWeatherReporter implements Observer  {

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof WeatherData) {
            var weatherData = (WeatherData) o;
            reportWeather(weatherData);
        }
    }

    protected abstract void reportWeather(WeatherData weatherData);
}
