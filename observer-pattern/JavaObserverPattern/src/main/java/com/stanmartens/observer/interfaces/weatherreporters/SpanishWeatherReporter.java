package com.stanmartens.observer.interfaces.weatherreporters;

import com.stanmartens.observer.interfaces.WeatherData;

public class SpanishWeatherReporter extends BaseWeatherReporter {

    @Override
    protected void reportWeather(WeatherData data) {
        System.out.println(String.format("La temperatura de hoy es %d °C.", data.getTemperature()));
    }
}
