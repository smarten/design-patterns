package com.stanmartens.observer.interfaces;

public class WeatherData {

    private int temperature;

    public WeatherData(int temperature) {
        this.temperature = temperature;
    }

    public int getTemperature() {
        return temperature;
    }
}
