package com.stanmartens.observer.builtinobserver;

public enum Season {
    SPRING,
    SUMMER,
    AUTUMN,
    WINTER
}
