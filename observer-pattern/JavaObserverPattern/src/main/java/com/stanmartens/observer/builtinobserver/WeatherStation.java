package com.stanmartens.observer.builtinobserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Random;

class WeatherStation extends Observable {

   private List<WeatherData> registeredWeatherReports;

   WeatherStation() {
      registeredWeatherReports = new ArrayList<>();
   }

   void releaseWeatherReport(Season season) {
      var random = new Random();
      int temperature = 0;

      switch (season) {
         case SPRING:
            temperature = random.nextInt(20);
            break;
         case SUMMER:
            temperature = random.nextInt(30);
            break;
         case AUTUMN:
            temperature = random.nextInt(10);
            break;
         case WINTER:
            temperature = random.nextInt(5);
            break;
      }

      var weatherData = new WeatherData(temperature);
      registeredWeatherReports.add(weatherData);

      this.setChanged();
      this.notifyObservers(weatherData);
   }
}
