package com.stanmartens.observer.builtinobserver.weatherreporters;

import com.stanmartens.observer.builtinobserver.WeatherData;

public class SpanishWeatherReporter extends BaseWeatherReporter {

    @Override
    protected void reportWeather(WeatherData weatherData) {
        System.out.println(String.format("La temperatura de hoy es %d °C.", weatherData.getTemperature()));

    }
}
