package com.stanmartens.observer.interfaces;

import com.stanmartens.observer.interfaces.weatherreporters.EnglishWeatherReporter;
import com.stanmartens.observer.interfaces.weatherreporters.SpanishWeatherReporter;

public class Main {
    public static void main(String[] args) {
        var spanishWeatherReporter = new SpanishWeatherReporter();
        var englishWeatherReporter = new EnglishWeatherReporter();

        var weatherStation = new WeatherStation();

        englishWeatherReporter.subscribe(weatherStation);
        spanishWeatherReporter.subscribe(weatherStation);

        weatherStation.measureWeather(Season.SPRING);

        englishWeatherReporter.unsubscribe();

        weatherStation.measureWeather(Season.SPRING);
    }
}
