package com.stanmartens.observer.builtinobserver;

import com.stanmartens.observer.builtinobserver.weatherreporters.EnglishWeatherReporter;
import com.stanmartens.observer.builtinobserver.weatherreporters.SpanishWeatherReporter;

public class Main {
    public static void main(String[] args) {
        var weatherStation = new WeatherStation();

        var englishReporter = new EnglishWeatherReporter();
        var spanishNewsReporter = new SpanishWeatherReporter();

        weatherStation.addObserver(englishReporter);
        weatherStation.addObserver(spanishNewsReporter);

        weatherStation.releaseWeatherReport(Season.SPRING);

        weatherStation.deleteObserver(englishReporter);

        weatherStation.releaseWeatherReport(Season.SUMMER);
    }
}
