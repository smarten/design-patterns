package com.stanmartens.observer.interfaces;

import java.util.List;

public class Unsubscriber<T> {

    private List<Observer<T>> registeredObservers;
    private Observer<T> observerToUnsubscribe;

    public Unsubscriber(List<Observer<T>> registeredObservers, Observer<T> observer) {
        this.registeredObservers = registeredObservers;
        this.observerToUnsubscribe = observer;
    }

    public void unsubscribe() {
        registeredObservers.remove(observerToUnsubscribe);
    }
}
