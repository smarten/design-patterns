package com.stanmartens.observer.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WeatherStation implements Observable<WeatherData> {

    private List<Observer<WeatherData>> observers;

    WeatherStation() {
        observers = new ArrayList<>();
    }

    @Override
    public Unsubscriber<WeatherData> subscribe(Observer<WeatherData> observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }

        return new Unsubscriber<>(this.observers, observer);
    }

    void measureWeather(Season season) {
        var random = new Random();

        var temperature = 0;
        switch (season) {
            case SPRING:
               temperature = random.nextInt(20);
               break;
            case SUMMER:
                temperature = random.nextInt(30);
                break;
            case AUTUMN:
                temperature = random.nextInt(10);
                break;
            case WINTER:
                temperature = random.nextInt(5);
                break;
        }

        var weatherData = new WeatherData(temperature);
        for (var observer : observers) {
            observer.updated(weatherData);
        }
    }
}
