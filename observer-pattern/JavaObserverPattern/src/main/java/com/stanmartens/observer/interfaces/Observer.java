package com.stanmartens.observer.interfaces;

public interface Observer<T> {

    void subscribe(Observable<T> observable);

    void unsubscribe();

    void updated(T args);
}
