package com.stanmartens.observer.interfaces.weatherreporters;

import com.stanmartens.observer.interfaces.Observable;
import com.stanmartens.observer.interfaces.Observer;
import com.stanmartens.observer.interfaces.Unsubscriber;
import com.stanmartens.observer.interfaces.WeatherData;

public abstract class BaseWeatherReporter implements Observer<WeatherData> {

    private Unsubscriber unsubscriber;

    @Override
    public void subscribe(Observable<WeatherData> observable) {
        unsubscriber = observable.subscribe(this);
    }

    @Override
    public void unsubscribe() {
        if (unsubscriber != null) {
            unsubscriber.unsubscribe();
        }
    }

    @Override
    public void updated(WeatherData args) {
        reportWeather(args);
    }

    protected abstract void reportWeather(WeatherData data);
}
