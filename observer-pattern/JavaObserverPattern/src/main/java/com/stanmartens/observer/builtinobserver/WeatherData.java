package com.stanmartens.observer.builtinobserver;

import java.time.LocalDateTime;

public class WeatherData {

    private int temperature;
    private LocalDateTime datePublished;

    public WeatherData(int temperature) {
        this.temperature = temperature;
        datePublished = LocalDateTime.now();
    }

    public int getTemperature() {
        return temperature;
    }

    public LocalDateTime getDatePublished() {
        return datePublished;
    }
}
