package com.stanmartens.observer.interfaces;

public interface Observable<T> {

    Unsubscriber subscribe(Observer<T> observer);
}
